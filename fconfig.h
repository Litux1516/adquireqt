﻿#ifndef FCONFIG_H
#define FCONFIG_H

#include <QDialog>

namespace Ui {
    class fConfig;
}

class fConfig : public QDialog {
    Q_OBJECT
public:
    fConfig(QWidget *parent = 0);
    fConfig(QString archName, QWidget *parent = 0);
    ~fConfig();

    int getTamBloque(){ return tamBloque;}
    QString getNombreArchivo();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::fConfig *ui;

    int tamBloque;

private slots:

    void saveValue(QString);
    void on_btnFile_clicked();


};

#endif // FCONFIG_H
