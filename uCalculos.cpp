﻿//---------------------------------------------------------------------------
 #include "uCalculos.h"
//---------------------------------------------------------------------------

void IniciarStrings() {

    //Logicos = new QStringList();
    Logicos << "Si";
    Logicos << "NO";

    //Ganancias = new QStringList();
    Ganancias << "x1";
    Ganancias << "x2";
    Ganancias << "x4";
    Ganancias << "x8";

    //Polaridad = new QStringLis();
    Polaridad << "Unipolar";
    Polaridad << "Bipolar";

    //Sensibilidades = new QStringList();
    Sensibilidades << "1";
    Sensibilidades << "1.5";
    Sensibilidades << "2";
    Sensibilidades << "3";
    Sensibilidades << "5";
    Sensibilidades << "7";
    Sensibilidades << "10";
    Sensibilidades << "15";
    Sensibilidades << "20";
    Sensibilidades << "30";
    Sensibilidades << "50";
    Sensibilidades << "70";

    //LO = new QStringList();
    LO << "0.1";
    LO << "0.3";
    LO << "1";
    LO << "5";

    //HI = new QStringList();
    HI << "15";
    HI << "35";
    HI << "70";
    HI << "300";
    HI << "1000";
    HI << "3000";
    HI << "10000";

    Graficar[0]= true;
    for (int n=1; n<MAXNUMEROCANALES; n++)
        Graficar[n]= false;
}


bool TextToBool(QString valor) {

    bool aux;
    valor=="Si" ? aux=true : aux =false;
    return aux;
}

DaqAdcGain TextToGanancia(QString valor) {

    DaqAdcGain aux=DgainX1;
    if(valor=="x1")
        aux=DgainX1;
    if(valor=="x2")
        aux=DgainX2;
    if(valor=="x4")
        aux=DgainX4;
    if(valor=="x8")
        aux=DgainX8;
    return aux;
}

DaqAdcFlag TextToPolaridad(QString valor) {

    DaqAdcFlag aux;
    if ( valor == Polaridad.first() )
        aux = (DaqAdcFlag)(DafUnipolar|DafAnalog);
    else
        aux = (DaqAdcFlag)(DafBipolar|DafAnalog);
    return aux;
}

QString PolaridadToText(DaqAdcFlag valor) {

    QString sale;
    valor == DafUnipolar ?  sale = Polaridad[0] : sale=  Polaridad[1];
    return sale;
}

int CantidadCanalesAGraficar() {

    int Cant=0;
    for(int n=0; n<MAXNUMEROCANALES; n++)
        if (Graficar[n])
            Cant++;

    return Cant;

}

QString BoolToText(bool valor) {
    QString aux;
    if (valor)
        aux="Si";
    else
        aux="NO";
    return aux;
}

QString GananciaToText(DaqAdcGain valor) {

    QString aux;
    if (valor==DgainX1)
        aux="x1";
    if (valor==DgainX2)
        aux="x2";
    if (valor==DgainX4)
        aux="x4";
    if (valor==DgainX8)
        aux="x8";
    return aux;
}


//---------------------------------------------------------------------------


