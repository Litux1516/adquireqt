﻿#include <QFileDialog>

#include "fconfig.h"
#include "ui_fconfig.h"

fConfig::fConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fConfig)
{
    ui->setupUi(this);
    tamBloque = 32;
}

fConfig::fConfig(QString archName, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fConfig)
{
    ui->setupUi(this);
    tamBloque = 32;
    ui->leNombreArchivo->setText(archName);
}

fConfig::~fConfig()
{
    delete ui;
}

void fConfig::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void fConfig::saveValue(QString valor){

    tamBloque = valor.toInt();
    if ( (tamBloque > 1025) || (tamBloque < 1) )
        tamBloque = 32;
}

QString fConfig::getNombreArchivo(){
  return ui->leNombreArchivo->text();
}

void fConfig::on_btnFile_clicked(){

    /*QFileDialog dialog;
    dialog.setFileMode(QFileDialog::AnyFile);
    if(dialog.exec())
        ui->leNombreArchivo->setText(dialog.FileName);
*/
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                "./",
                                tr("Binary (*.dat *.mat)"));
    ui->leNombreArchivo->setText(fileName);
}
