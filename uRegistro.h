//---------------------------------------------------------------------------

#ifndef uRegsitroH
#define uRegsitroH

#include "uDaqx.h"

//---------------------------------------------------------------------------
class Registro {

public:
    DaqX *DaqB2K;
    Registro();
    ~Registro();

    QString estado;

    void setFrecuencia(unsigned frec = 500);
    void setCanales(unsigned valor = 1);
    void setGanancia(QString valor);
    void setPolaridad(QString valor);
    void setTamBloque(unsigned valor = 32);

    unsigned getFrecuencia();
    unsigned getCanales();
    unsigned getTamBloque();

    bool iniciar();
    void configurarPlaca(QString nomArch);
    void play();
    void replay();
    void pause();
    void stop();
    void finalizar();

private:
     bool placaLista();
};
//---------------------------------------------------------------------------
#endif
