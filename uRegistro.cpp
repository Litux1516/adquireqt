#include <QDebug>
#include <QFileInfo>

#include "uRegistro.h"
#include "uDaqx.h"

//---------------------------------------------------------------------------
Registro::Registro() {
    DaqB2K = new DaqX;

}

//---------------------------------------------------------------------------
Registro::~Registro(){
}

//---------------------------------------------------------------------------
bool Registro::placaLista() {
    bool det=false;
    if ( DaqB2K->detecto() )
        det = true;
    return det;
}

//---------------------------------------------------------------------------
void Registro::configurarPlaca(QString nomArch) {

    QFileInfo info(nomArch);
    int count=0;
    QString aux = info.baseName();

    while( info.exists() ){

        info.setFile(info.absolutePath()+"/"
                     +aux+QString::number(count++)
                     +"."+info.suffix()
                     );
    }

    DaqB2K->Archivo_Salida(info.filePath());

    if ( DaqB2K->configurar() == DerrNoError )
        qDebug() << "Placa Configurada!!";

    else {
        qDebug() << "Placa No Lista";
        exit(1);
    }

}

//---------------------------------------------------------------------------
void Registro::setFrecuencia(unsigned frec) {
    //set scan rate
    DaqB2K->setFrecuencia( frec );
    qDebug( "Frecuencia igual a : %d", frec);
}

//---------------------------------------------------------------------------
void Registro::setCanales(unsigned valor) {
    //Cantidad de Canales
    DaqB2K->setCanales( valor );
    qDebug( "Canales igual a : %d", valor);
}

//---------------------------------------------------------------------------
void Registro::setGanancia(QString valor) {
    //Ganancia de todos los canales


}

//---------------------------------------------------------------------------
void Registro::setPolaridad(QString valor) {

}

//---------------------------------------------------------------------------
void Registro::setTamBloque(unsigned valor){

    DaqB2K->setTamBloque(valor);
    qDebug( "Tamaño bloque: %d", valor );

}

//---------------------------------------------------------------------------
unsigned Registro::getFrecuencia() {
    return DaqB2K->getFrecuencia();
}

//---------------------------------------------------------------------------
unsigned Registro::getCanales() {
    return DaqB2K->getCanales();
}

//---------------------------------------------------------------------------
unsigned Registro::getTamBloque() {
    return DaqB2K->getTamBloque();
}

//---------------------------------------------------------------------------
bool Registro::iniciar(){
    return DaqB2K->detecto();
}
//---------------------------------------------------------------------------
void Registro::play(){

    if ( DaqB2K->armar() == DerrNoError){
        DaqB2K->adquirir();
        DaqB2K->start();
        qDebug() << "Adquiriendo!!";
    }
    else{
        qDebug() << "No se logro armar el hardware";
        exit(1);
    }

}

//---------------------------------------------------------------------------
void Registro::pause(){

    DaqB2K->stop();

}

//----------------------------------------------------------------------------
void Registro::replay(){

    DaqB2K->start();

}

//---------------------------------------------------------------------------
void Registro::stop(){

    pause();
    DaqB2K->desarmar();

}

//---------------------------------------------------------------------------
void Registro::finalizar() {

    pause();
    DaqB2K->desarmar();
    qDebug() << "Adquisición detenida...";
    DaqB2K->cerrar();

}

