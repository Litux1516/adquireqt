﻿
//---------------------------------------------------------------------------
#ifndef uCalculosH
#define uCalculosH

#include <QStringList>
#include "Daqx.h"

#ifndef MAXNUMEROCANALES
#define MAXNUMEROCANALES 16
#endif


//---------------------------------------------------------------------------
   QStringList Logicos;
   QStringList Ganancias;
   QStringList Polaridad;
   QStringList Sensibilidades;
   QStringList LO;
   QStringList HI;
    bool Graficar[16];


    QString PathFile;

    bool TextToBool(QString valor);
    DaqAdcGain TextToGanancia(QString valor);
    DaqAdcFlag TextToPolaridad(QString valor);

    QString PolaridadToText(DaqAdcFlag valor);
    QString BoolToText(bool valor);
    QString GananciaToText(DaqAdcGain valor);

    int CantidadCanalesAGraficar();

    void IniciarStrings();


//---------------------------------------------------------------------------
#endif

