# -------------------------------------------------
# Project created by QtCreator 2009-10-28T16:16:56
# -------------------------------------------------
TARGET = adquireQT
TEMPLATE = app
DEPENDPATH += ./basicgraph

INCLUDEPATH += daqBoard_SDK/Include \
               ./basicgraph \
               biosig4c++/includes \
               matlab/includes

include(basicgraph/basicgraph.pri)
include(5waybutton/5waybutton.pri)
include(common/common.pri)

LIBS += daqBoard_SDK/Lib/DAQX.lib \
        biosig4c++/lib/libbiosig.dll \
        matlab/lib/gcc/libmat.lib \
        #matlab\lib\gcc\libmex.lib \
       matlab/lib/gcc/libmx.lib

#CONFIG += debug
QT += opengl

SOURCES += main.cpp \
    fprincipal.cpp \
    uCalculos.cpp \
    uDaqx.cpp \
    uRegistro.cpp \
    daqBoard_SDK/Include/DaqRoutines.cpp \
    fconfig.cpp

HEADERS += fprincipal.h \
    uCalculos.h \
    uDaqx.h \
    uRegistro.h \
    daqBoard_SDK/Include/DaqRoutines.h \
    daqBoard_SDK/Include/Daqx.h \
    biosig4c++/includes/biosig.h \
    matlab/includes/mat.h \
    fconfig.h

FORMS += fprincipal.ui \
         fconfig.ui
