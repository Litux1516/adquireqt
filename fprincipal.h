﻿#ifndef FPRINCIPAL_H
#define FPRINCIPAL_H

#include <QtGui/QMainWindow>
#include <QStackedWidget>
#include <QVector>
#include <QMap>
#include "uRegistro.h"

#define Visual 2

class QtBasicGraph;

namespace Ui
{
    class fPrincipal;
}

enum Estado{Adquire, Stop, Pause};

class fPrincipal : public QMainWindow
{
    Q_OBJECT

public:
    fPrincipal(QWidget *parent = 0);
    ~fPrincipal();

private:
    Ui::fPrincipal *ui;
    Registro reg;

    Estado estado;

    QMap<int, QVector<QPointF> > curveMap;

    QList<QtBasicGraph*> chanelList;
    void initPlot();

    void writeSettings();
    void readSettings();
    void stateMachine(Estado state);

    int activeChannels;
    int ticks, pasoMuestras;
    float zoom;
    QString nombreArchivo;


public slots:
        void setNewFreq(int value);
        void setNewCantCana(int value);
        void on_btnGrab_clicked(bool);

        void adquirir();
        void pausa();
        void stop();
        void on_btnConfig_clicked();


        void actualizarPlot(double *values);

        void moveUp();
        void moveDown();
        void restore();
        void moveLeft();
        void moveRight();


};

#endif // FPRINCIPAL_H
