#include <QtGlobal>
#include <QDebug>

#include "fprincipal.h"
#include "ui_fprincipal.h"
#include "uDaqx.h"
#include "fconfig.h"

//---------------------------------------------------------------------------
/*!
  @brief Inicializa todas las variables

  Inicializa las variables y relaiza las conecciones entre señales y slots.
  Además setea compotamientos de los componentes graficos.
*/
fPrincipal::fPrincipal(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::fPrincipal), estado(Stop)
{
    ui->setupUi(this);

    connect(ui->btnPlay, SIGNAL(clicked()), this, SLOT(adquirir()));
    connect(ui->btnPausa, SIGNAL(clicked()), this, SLOT(pausa()));
    connect(ui->btnStop, SIGNAL(clicked()), this, SLOT(stop()));

    connect(reg.DaqB2K, SIGNAL(datosAdquiridos(double *)),
            this, SLOT(  actualizarPlot(double *) ) );

    connect(ui->btnGrab, SIGNAL(clicked(bool)), reg.DaqB2K, SLOT(saveFile(bool)) );
    ui->btnGrab->setText("No grabando :_(");

    connect(ui->actionAcerca_de_QT, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(ui->action_Salir, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionConfigurar, SIGNAL(triggered()), this, SLOT(on_btnConfig_clicked()));

    ui->btnPlay->setEnabled(true);
    ui->btnPausa->setEnabled(false);
    ui->btnStop->setEnabled(false);

    ui->btnZoom->setSkin("Beryl");

    ui->statusBar->showMessage("180 Muestras, Rango +-10");
    connect(ui->btnZoom->upButton(), SIGNAL(clicked()), this, SLOT(moveUp()));
    connect(ui->btnZoom->downButton(), SIGNAL(clicked()), this, SLOT(moveDown()));
    connect(ui->btnZoom->centerButton(), SIGNAL(clicked()), this, SLOT(restore()));
    connect(ui->btnZoom->leftButton(), SIGNAL(clicked()), this, SLOT(moveLeft()));
    connect(ui->btnZoom->rightButton(), SIGNAL(clicked()), this, SLOT(moveRight()));

    zoom = 1.1;
    pasoMuestras = 10;

    initPlot();
    readSettings();
    nombreArchivo = QDir::currentPath()+"data\S000";

    QVector<QPointF> data;
    curveMap[1]= data;
    activeChannels = 0;
    chanelList.clear();
    chanelList.push_back( ui->plotGraph );

    if ( !reg.iniciar() ) ui->btnPlay->setEnabled(false);
}

//---------------------------------------------------------------------------
/*!
  @brief Destructor

  Guarda los seteos y libera la memoria.
*/
fPrincipal::~fPrincipal()
{
    writeSettings();
    delete ui;
}

//---------------------------------------------------------------------------
/*!
  @brief Inicializa la ventana de graficacion

  --.
*/
void fPrincipal::initPlot(){
  //ui->plotGraph->setRenderHints(QPainter::Antialiasing);
  ui->plotGraph->setYMinMax(-10.0,10.0);
  ui->plotGraph->setXRange(180.0);
  ui->plotGraph->setFocusPolicy(Qt::NoFocus);


}

//---------------------------------------------------------------------------
void fPrincipal::setNewCantCana(int value){

    if ( (value -1) > activeChannels){
        QtBasicGraph *newCana;
        newCana = new QtBasicGraph(this);


        newCana->setObjectName("plotGraph"+QString::number(value));
        //newCana->setRenderHints(QPainter::Antialiasing);
        newCana->setYMinMax(-10.0,10.0);
        newCana->setXRange(180.0);
        newCana->setFocusPolicy(Qt::NoFocus);
        newCana->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

        ui->plotGraphLayout->addWidget(newCana);
        chanelList.push_back(newCana);
    }
    else{
        QtBasicGraph *newCana;
        newCana = chanelList.last();
        ui->plotGraphLayout->removeWidget(newCana);
        chanelList.pop_back();
    }


    activeChannels = value -1;

}

//---------------------------------------------------------------------------
void fPrincipal::setNewFreq(int value){

  if( (estado!=Adquire) && (estado!=Pause) ){
    reg.setFrecuencia(value);
  }

}

//---------------------------------------------------------------------------
void fPrincipal::on_btnGrab_clicked(bool value){

  value ? ui->btnGrab->setText("GRABANDO :)") : ui->btnGrab->setText("No grabando :_(");
}

//------------- slots de adquisicion ---------------------------------
void fPrincipal::adquirir(){

    estado = Adquire;

    stateMachine(estado);

    ui->btnPlay->setEnabled(false);
    ui->btnPausa->setEnabled(true);
    ui->btnStop->setEnabled(true);
}

//---------------------------------------------------------------------------
void fPrincipal::pausa(){

  estado = Pause;
  stateMachine(estado);
  ui->btnPlay->setEnabled(true);
  ui->btnPausa->setEnabled(false);
  ui->btnStop->setEnabled(true);

}

//---------------------------------------------------------------------------
void fPrincipal::stop(){

  estado = Stop;
  stateMachine(estado);
  for (unsigned n=0; n<reg.getCanales(); n++)
    chanelList[n]->clear();

  ui->btnPlay->setEnabled(true);
  ui->btnPausa->setEnabled(false);
  ui->btnStop->setEnabled(false);

}

//---------------------------------------------------------------------------
/*!
  @brief Habre el cuadro de dialogo de configuracion

  En el cuadro de configuracion se setean parametros que no son
  explusivos de la adquisicion, como nombre de archivos y otros.
  Recuerda valores anteriores.
*/
void fPrincipal::on_btnConfig_clicked(){

    fConfig *ConfigDiag;
    QFile archi(nombreArchivo);
    if (archi.exists())
      ConfigDiag = new fConfig(nombreArchivo, this);
    else
      ConfigDiag = new fConfig(this);

    ConfigDiag->exec() ;
    if ( ConfigDiag->result() )  {
      reg.setTamBloque( ConfigDiag->getTamBloque() );
      nombreArchivo = ConfigDiag->getNombreArchivo();
  }

}

//---------------------------------------------------------------------------
void fPrincipal::actualizarPlot(double *values){

  QPointF pt(0,0);

  for( unsigned n=0; n < reg.getTamBloque(); n++ ){
      pt.setX(ticks++);
      for(unsigned l=0; l<reg.getCanales(); l++){
          pt.setY( values[ (reg.getCanales()*n)+l  ] );
          chanelList[l]->addPoint(pt);
      }
  }

//  for (int n = 0; n < reg.getTamBloque(); ++n ){
//      pt.setX(ticks++);
//      pt.setY( values[n] );
//      chanelList[0]->addPoint(pt);
//      //ui->plotGraph->addPoint(pt);
//      //curveMap.value(1).push_back(pt);
//
//  }

}

//---------------------------------------------------------------------------
void fPrincipal::writeSettings()
 {
     QSettings settings(QSettings::IniFormat, QSettings::UserScope, "NeuruS Soft", "AdquireQt");

     settings.beginGroup("adquisicion");
     settings.setValue("tamBloque", reg.getTamBloque() );
     settings.setValue("frecuencia", reg.getFrecuencia() );
     settings.setValue("tamBloque", reg.getTamBloque() );
     settings.setValue("nombreArchivo", nombreArchivo);
     settings.endGroup();
 }

//---------------------------------------------------------------------------
void fPrincipal::readSettings()
{
     QSettings settings(QSettings::IniFormat, QSettings::UserScope, "NeuruS Soft", "AdquireQt");

     settings.beginGroup("adquisicion");
     reg.setFrecuencia( settings.value("frecuencia", 500).toUInt() ) ;
     reg.setCanales( settings.value("canales", 1).toUInt() );
     reg.setTamBloque( settings.value("tamBloque", 32).toUInt() );
     nombreArchivo = settings.value("nombreArchivo").toString();
     settings.endGroup();
}

//---------------------------------------------------------------------------
void fPrincipal::stateMachine(Estado state){

   switch (state){

       case Adquire: {
               reg.iniciar();
               reg.setCanales( ui->spBCanales->value() );
               reg.setFrecuencia( ui->spBFreq->value() );
               reg.configurarPlaca(nombreArchivo);
               qDebug( "Adquiriendo" );
               reg.play(); break;
               break;
       }

       case Stop: {
               reg.finalizar();
               break;
       }

       case Pause: {
               reg.pause();
               break;
       }

   }

}

//---------------------------------------------------------------------------
void fPrincipal::moveUp(){

     float ymx = chanelList[0]->yMax();
     float ymn = chanelList[0]->yMin();
     ymx /= zoom;
     ymn /= zoom;

     for(int n=0; n<chanelList.count(); n++)
         chanelList[n]->setYMinMax(-ymn,ymx);

     QString mensage= QString::number(chanelList[0]->xRange())+" Muestras, Rango +-" + QString::number(ymn);
     ui->statusBar->showMessage(mensage);

}

//---------------------------------------------------------------------------
void fPrincipal::moveDown(){

     float ymx = chanelList[0]->yMax();
     float ymn = chanelList[0]->yMin();
     ymx *= zoom;
     ymn *= zoom;

     for(int n=0; n<chanelList.count(); n++)
         chanelList[n]->setYMinMax(-ymn,ymx);

     QString mensage= QString::number(chanelList[0]->xRange())+" Muestras, Rango +-" + QString::number(ymn);
     ui->statusBar->showMessage(mensage);

 }

//---------------------------------------------------------------------------
 void fPrincipal::restore(){

     for(int n=0; n<chanelList.count(); n++){
         chanelList[n]->setYMinMax(-10.0,10.0);
         chanelList[n]->setXRange(180);
     }

     QString mensage= QString::number(chanelList[0]->xRange())+" Muestras, Rango +-"
                    + QString::number(chanelList[0]->yMax());
     ui->statusBar->showMessage(mensage);

 }

 //---------------------------------------------------------------------------
 void fPrincipal::moveLeft(){

     int r = chanelList[0]->xRange();
     r += pasoMuestras;
     for(int n=0; n<chanelList.count(); n++)
         chanelList[n]->setXRange(r);

     QString mensage= QString::number(chanelList[0]->xRange())+" Muestras, Rango +-"
                    + QString::number(chanelList[0]->yMax());
     ui->statusBar->showMessage(mensage);

 }

 //---------------------------------------------------------------------------
 void fPrincipal::moveRight(){

     int r = chanelList[0]->xRange();
     r -= pasoMuestras;
     for(int n=0; n<chanelList.count(); n++)
         chanelList[n]->setXRange(r);

     QString mensage= QString::number(chanelList[0]->xRange())+" Muestras, Rango +-"
                    + QString::number(chanelList[0]->yMax());
     ui->statusBar->showMessage(mensage);

 }


