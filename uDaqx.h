﻿//---------------------------------------------------------------------------
#ifndef uDaqXH
#define uDaqXH

#include <QObject>
#include <QVector>
#include <QList>
#include <QTimer>
#include <QFile>

#include "Daqx.h"
#include "DaqRoutines.h"
#include "biosig.h"
#include <mat.h>


#define STARTSOURCE     DatsSoftware
#define STOPSOURCE	DatsScanCount
#define TRIGSENSE	DetsRisingEdge
#define CANTCANA	16
#define SCANS		10      //1 meaningless in an infinite scan
#define RATE		1000
#define NOM_ARCH	"Daq2K.bin"
#define PREWRITE        0
//---------------------------------------------------------------------------

struct Canal {
    DaqAdcFlag Polaridad;
    DaqAdcGain Ganancia;
    bool Activo;
};


class DaqX : public QObject{
    Q_OBJECT

public:

    DaqX();
    ~DaqX();

    DaqError err;
    QVector <Canal> CanalesActivos;

    void setMessage(const QString &message);
    void setCanales(int valor);
    void setFrecuencia(int valor);
    void setTamBloque(int valor);

    int getCanales() { return FCantCanales; };
    int getFrecuencia() { return FFrecuencia; };
    int getTamBloque() { return FTamBloque; };

    bool detecto();
    DaqError configurar();
    DaqError armar();
    DaqError adquirir();
    DaqError desarmar();
    DaqError cerrar();

    void Archivo_Salida(QString arc_sal);
    void stop();
    void start();

private:

    bool stopped;

    DaqHandleT handle;
    WORD buffer[1024];
    DaqAdcGain gains[CANTCANA];
    DWORD channels[CANTCANA];
    DWORD flags[CANTCANA];

    unsigned int FCantCanales;
    unsigned int FFrecuencia;
    unsigned int FTamBloque;
    DaqAdcGain   FGanancias;
    DWORD FPolaridad;

    //used to monitor scan
    DWORD scansCollected;
    DWORD compMask;
    float maxVolt;
    float scale[CANTCANA];
    float offset[CANTCANA];
    double conv_buffer[1024];

    QString archivo_texto;
    QFile ouput;
    HDRTYPE *ouputHdr;
    MATFile *pmat;
    mxArray *pa1;
    bool matFile;

    char* devName;   	//used to connect to device

    bool conectada;
    bool terminar;
    bool save;

    int scans;

    QTimer *qtimer;
    QVector <DaqAdcGain> Ganancias;
    QVector <DaqAdcFlag> Polaridades;

    unsigned int DetActivos();
    void escalaSenal();
    void cabeceraArchivo();


private slots:
    void update();
    void saveFile(bool value);

signals:
    void datosAdquiridos(double *values);

};

#endif




