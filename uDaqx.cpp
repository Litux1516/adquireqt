//---------------------------------------------------------------------------
#include <QtGui/QApplication>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <QDebug>

#include <math.h>
#include <mat.h>

#include "DaqRoutines.h"
#include "uDaqx.h"


//---------------------------------------------------------------------------
DaqX::DaqX() {

    scansCollected=0;
    scans=0;
    FCantCanales = 1;
    FFrecuencia = 500;
    FTamBloque = 32;

    save = false;
    qtimer = new QTimer();
    connect(qtimer, SIGNAL(timeout()), this, SLOT(update()));

   /* gains.resize(CANTCANA);
    flags.resize(CANTCANA);
    channels.resize(CANTCANA);

    gains.fill(DgainX1);
    flags.fill( (DaqAdcFlag)(DafBipolar|DafAnalog) );*/

    CanalesActivos.resize(CANTCANA);
    Ganancias.resize(CANTCANA);
    Polaridades.resize(CANTCANA);

    for(int n=0; n<CANTCANA;n++) {
        channels[n] = n;
        CanalesActivos[n].Activo=false;
        CanalesActivos[n].Ganancia=DgainX1;
        CanalesActivos[n].Polaridad= (DaqAdcFlag)(DafUnipolar|DafAnalog);

        gains[n] = DgainX1;
        flags[n] = (DaqAdcFlag)(DafBipolar|DafAnalog) ;
    }

    matFile = false;

}

//---------------------------------------------------------------------------
DaqX::~DaqX(){

  handle=-1;
  mxDestroyArray(pa1);
}

//---------------------------------------------------------------------------
void DaqX::escalaSenal() {

    //----------- Para escalar la senal --------------------
    if (compMask & daq10V)	//Max voltage for Daq2K is +/-10V
        maxVolt = 10.0;
    else			       //Max voltage for Legacy is +/-5V
        maxVolt = 5.0;

    //scale.resize(FCantCanales);
    //offset.resize(FCantCanales);
    for(unsigned g = 0; g < FCantCanales; g++) {
        //(flag&DafBiplor) equals 2 if bipolar or 0 if unipolar
        //scale should equal maxVolt/65335 for unipolar, maxVolt/32767 for bipolar
        scale[g]=(maxVolt/(65535 - (flags[g]&DafBipolar)*16384));
        //offset should equal maxVolt for bipolar, 0 for unipolar
        offset[g] = (maxVolt*(flags[g]&DafBipolar)/2);
    }

}

//---------------------------------------------------------------------------
void DaqX::setCanales(int valor){
    if ( (valor > 0) && (valor < 17) )
        FCantCanales = valor;
    else
        FCantCanales = 1;

}

//---------------------------------------------------------------------------
void DaqX::setFrecuencia(int valor){
    if ( (valor > 0) && (valor < 200001) )
        FFrecuencia = valor;
    else
        FFrecuencia = 1;

}

//---------------------------------------------------------------------------
void DaqX::setTamBloque(int valor){
    if ( (valor > 0) && (valor < 1025) )
        FTamBloque = valor / FCantCanales;
    else
        FTamBloque = 32;

}

//---------------------------------------------------------------------------
bool DaqX::detecto() {

    bool deteccion=false;
    //lista de dispositivos compatibles, ver DaqRoutines.h
    compMask = daq2000AI|daqboard|daqbook|daq216|daqPCcard;

    //this code will poll for the first compatible device
    //if the preferred device name is known, set devName equal here
    devName = GetDeviceName(compMask);  //found in DaqRoutines.cpp
    if (*devName != NULL) {
        deteccion = true;
        handle = daqOpen(devName);
    }

    return deteccion;
}

//---------------------------------------------------------------------------
DaqError DaqX::configurar() {
    BOOL online=false;
    daqOnline(handle, &online);

    if(online){
        try {

            //buffer.resize(SCANS*FCantCanales);
            //conv_buffer.resize(SCANS*FCantCanales);

            //---------------------- forma Infinite Pos ------------
            // set # of scans to perform and scan mode
            daqAdcSetAcq(handle, DaamInfinitePost, 0, 0);
            //Scan settings
            for (unsigned n=0; n<FCantCanales; n++)
                channels[n] = n;

            //Set buffer size and flag settings
            err=daqAdcSetScan(handle, channels, gains, flags, FCantCanales);

            //set scan rate
            err=daqAdcSetFreq(handle, FFrecuencia);
            //for circular driver buffer, set location to NULL, and set a size larger than the number of scans per second
            err=daqAdcTransferSetBuffer(handle, NULL, FFrecuencia*FTamBloque*2, DatmCycleOn|DatmUpdateSingle|DatmDriverBuf);

            //Set to Trigger Immediatly
            daqSetTriggerEvent(handle, STARTSOURCE, TRIGSENSE, channels[0], gains[0], flags[0], DaqTypeAnalogLocal, NULL, NULL, DaqStartEvent);
            //Set to Stop when scan is complete
            daqSetTriggerEvent(handle, STOPSOURCE, TRIGSENSE, channels[0], gains[0], flags[0], DaqTypeAnalogLocal, NULL, NULL, DaqStopEvent);
            qDebug() << "Placa configurada!";
        } catch(...) {
            qFatal("Hubo problemas al configurar el hardware");
        }
    }
    else
        qFatal("El hardware no está listo :_(");

    return err;
}

//---------------------------------------------------------------------------
DaqError DaqX::armar() {
    //begin data acquisition

    err=daqAdcTransferStart(handle);
    err=daqAdcArm(handle);
    return err;
}

//---------------------------------------------------------------------------
DaqError DaqX::adquirir() {

    err=daqAdcSoftTrig(handle);
    escalaSenal();

    return err;
}

//---------------------------------------------------------------------------
void DaqX::update() {

    static unsigned long count = 0;

    daqAdcTransferBufData(handle, buffer, FTamBloque, DabtmWait, &scansCollected);
    //converted to volt
    for (WORD t = 0; t < scansCollected*FCantCanales; t++){
      conv_buffer[t] = (buffer[t]*scale[t%FCantCanales] - offset[t%FCantCanales]);
     }

    if (matFile&&save){
        memcpy((void *)(mxGetPr(pa1)), (void *)conv_buffer, scansCollected*sizeof(conv_buffer[0]));
        QString aux = "Bloque"+QString::number(count++);
        matPutVariable(pmat, aux.toAscii(), pa1);
    }else if(save)
        ouput.write( (char*)&conv_buffer[0], scansCollected*sizeof(conv_buffer[0]) );

    emit  datosAdquiridos(&conv_buffer[0]);

    //QApplication::processEvents();

  //  qDebug() << "adquiridos " << scansCollected;

}

//---------------------------------------------------------------------------
DaqError DaqX::desarmar(){

    return daqAdcDisarm(handle);
}

//---------------------------------------------------------------------------
DaqError DaqX::cerrar(){
    DaqError err;
    err = daqClose(handle);
    if ( ouput.isOpen() ) ouput.close();
    //sclose( ouputHdr );
    if ( matFile ) matClose(pmat);
    handle = -1;
    return err;
}

//---------------------------------------------------------------------------
void DaqX::stop(){

   qtimer->stop();

}

//---------------------------------------------------------------------------
void DaqX::start(){

   qtimer->start();

}

//---------------------------------------------------------------------------
/*!
   Llamar esta funcion antes que configurar placa SIEMPRE!!!
 */
void DaqX::Archivo_Salida(QString arc_sal){

    archivo_texto = arc_sal;
    QFileInfo info(arc_sal);
    if(info.suffix() == "mat") matFile = true;
    if(info.suffix() == "dat") matFile = false;
    if(info.suffix().isEmpty() ){
        //info.suffix() = "mat";
        matFile = true;
        archivo_texto+=".mat";
    }

    if(!matFile){
        ouput.setFileName(archivo_texto);
        ouput.open(QIODevice::WriteOnly);

        //ouputHdr = constructHDR(0,0);
        //ouputHdr->FileName = archivo_texto.toAscii();
        //sopen( archivo_texto.toAscii(), "w", ouputHdr );
    }
    else {
        pmat = matOpen(archivo_texto.toAscii(), "w");
        pa1 = mxCreateDoubleMatrix(1,FTamBloque,mxREAL);
    }

    cabeceraArchivo();
}

void DaqX::cabeceraArchivo(){

    if (matFile){
        mxArray *pa3;
        pa3 = mxCreateString("LIRINS: Comprometidos con la excelencia");
        matPutVariable(pmat, "Mensaje", pa3);

        mxArray *sclAr;
        sclAr = mxCreateDoubleScalar(FFrecuencia);
        matPutVariable(pmat, "Frecuencia", sclAr);
        sclAr = mxCreateDoubleScalar(FCantCanales);
        matPutVariable(pmat, "Canales", sclAr);
        sclAr = mxCreateDoubleScalar(FTamBloque);
        matPutVariable(pmat, "TamBloque", sclAr);

        mxDestroyArray(pa3);
        mxDestroyArray(sclAr);
    }
    else{
        if ( ouput.isOpen() ){
            double aux = FFrecuencia;
            ouput.write( (char*)&aux, sizeof(aux) );
            aux = FCantCanales;
            ouput.write( (char*)&aux, sizeof(aux) );
            aux = FTamBloque;
            ouput.write( (char*)&aux, sizeof(aux) );
        }
    }
}

void DaqX::saveFile(bool value){
    save=value;
}
