/* MultipleDaqBoard2000_Ex01.CPP

 32-bit enhanced API  

 This example demonstrates device initialization and detection.
 All communication through the driver is done using a device handle.
 The handle is returned from the daqOpen API call.
 Valid handles range from 0 up to the max number of devices supported.
 Only one application can have access to a device at a time.
 daqOpen will fail only if the device is not configured in the control
 panel, configued incorrectly, or not connected or powered.
 daqOpen requires the device name string assigned to the device through
 the Daq* Configuration applet in the Windows Control Panel.
 To auto-detect device names, daqGetDeviceList will return an array of
 configured device names.
 These names can then be used with daqGetDeviceProperties to determine
 what type of device it is configured as.
 daqOpen must then be called to verify communication and return the handle.

 Functions used:
 daqGetDeviceCount( &deviceCount);
 daqGetDeviceList( DaqDeviceList );
 daqGetDeviceProperties( daqName, &devProps);
 daqOpen(handle);	
 daqClose(handle);
 daqAdcSetAcq(handle, mode, preTrigCount, postTrigCount);
 daqAdcSetScan(handle, &channels, &gains, &flags, chanCount);
 daqAdcSetFreq(handle, freq );
 daqSetTriggerEvent(handle, trigSource, trigSensitivity,
						  channel, gainCode, flags, channelType,
						  level, variance, trigevent);					
 daqAdcTransferSetBuffer(handle, buf, scanCount,transferMask);
 daqAdcTransferStart(DaqHandleT handle);
 daqAdcTransferGetStat(DaqHandleT handle, active, retCount);
 daqAdcSetClockSource(DaqHandleT handle, DaqAdcClockSource clocksource);
 daqAdcArm(handle);
 daqAdcDisarm(handle);*/

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>
#include <string.h>
#include "daqx.h"

#define STARTSOURCE	DatsImmediate	
#define STOPSOURCE	DatsScanCount
#define CHANCOUNT	2
#define SCANS		10
#define RATE		1000	// In Hz
#define OUTPUTFILE	"DaqBoard2K"
#define OUTPUTFILE_SYNCH	"DaqBoards"
char file_name[25];
struct daqDevice
{
	DaqHandleT	handle;		// Device handle
	DWORD		devType;	// Used in scaling to SI units
	DWORD		active,		// Used to monitor scan
				retCount;
				
	char		devName[64];
};

// device type strings for display
char* daqTypeString[] = 
{
   "DaqBook100",
   "DaqBook112",
   "DaqBook120",
   "DaqBook200",
   "DaqBook216",
   "DaqBoard100",
   "DaqBoard112",
   "DaqBoard200",
   "DaqBoard216",
   "Daq112",
   "Daq216",
   "WaveBook512",
   "WaveBook516",
   "TempBook66",
   "PersonalDaq56",
   "WaveBook516_250",
   "WaveBook512_10V",
   "DaqBoard2000",
   "DaqBoard2001",
   "DaqBoard2002",
   "DaqBoard2003",
   "DaqBoard2004",
   "DaqBoard2005",
   "Unknown Device",
};

void main()
{
   DaqHandleT        handle;
   DaqDeviceListT    *devList;
   DaqDevicePropsT   devProps;
   DWORD             devCount = 0, deviceIndex = 0;
   DaqError          dError;

   // Used to monitor scan   
   DWORD	allActive=0, totCount=0;
	
/******************************************************************************/
/*							DEVICE DETECT and DISPLAY						  */
/******************************************************************************/   
	// Display Title
	printf("DaqBoard 2000 Multi-Board Detection and Scan Example.\n\n\n");

	// For the demonstration we will disable default error handling.
	dError = daqSetDefaultErrorHandler(NULL);

	// Find out how many DaqX devices are configured in the registry	
	daqGetDeviceCount(&devCount);
   
	// Convert devCount to int idevCount for loop index
	int idevCount = static_cast<int>(devCount);
   
	if (devCount == 0)
	{   
      // No devices found; exit program
      printf("\nNo registered DaqX devices found!\n");
      printf("Please revisit the Daq* Configuration applet in the Control Panel.\n");
      printf("\n\n<hit any key to exit>\n");
      _getch();
	}

	// show the device count
	printf("Detected %d registered DaqX devices:\n\n",devCount);

	// Allocate enough memory for the device list
	devList = (DaqDeviceListT*)malloc(sizeof(DaqDeviceListT)*devCount);
   
	// Fill the device list with the names of all installed devices and the device count (again).
	daqGetDeviceList(devList, &devCount);

	// Display properties' header columns
	printf("Device Type\tAD\tDA\tDI\tDO\tCTR\tName\n");
	printf("-------------------------------------------------------------------\n");
	
	/* Setup daqboard 2003 array to store string compare value
	   (2003 should not be in the scan list but if it is, display message)*/
	int iDaqBoard2003[4];

	/* Loop through devList and display all devices found and their associated properties
       These properties can be used to determine which device to open in a multi-device system.
       Device type or features/functionality can be the deciding factors.*/
	for (deviceIndex = 0; deviceIndex < devCount; deviceIndex++)
	{
      	daqGetDeviceProperties(devList[deviceIndex].daqName, &devProps);
		
		// error check for possible newer devices than example.
		if (devProps.deviceType > (DaqHardwareVersion)((sizeof(daqTypeString)/sizeof(daqTypeString[0])) -1))
			devProps.deviceType = (DaqHardwareVersion)((sizeof(daqTypeString)/sizeof(daqTypeString[0])) -1);
		
			printf("%s\t%2d\t%2d\t%2d\t%2d\t%2d\t%s\n",
			daqTypeString[devProps.deviceType],
			devProps.mainUnitAdChannels,
			devProps.mainUnitDaChannels,
			devProps.mainUnitDigInputBits,
			devProps.mainUnitDigOutputBits,
			devProps.mainUnitCtrChannels,         
			devProps.daqName);
			// Look in devList for DaqBoard 2003
			iDaqBoard2003[deviceIndex] = strcmp(daqTypeString[devProps.deviceType], "DaqBoard2003");
	}

	printf("\n");

	// Loop iDaqBoard2003 array - if exists print message
	for (deviceIndex = 0; deviceIndex < devCount; deviceIndex++)
	{
		
		if (iDaqBoard2003[deviceIndex] == 0)
		{
			printf("Your device list includes a DaqBoard 2003.\n");
			printf("The DaqBoard 2003 has no analog or digital inputs.\n");
			printf("Files created will contain no data.\n");
			printf("\n<hit any key to continue>\n\n");
			 _getch();
		}
	}

   // Loop through and connect to device(s) from the list.
   for (int i = 0; i < idevCount; i++)
   {
	   printf("Attempting to initialize detected device name: %s.\n",devList[i].daqName);
	   handle = daqOpen(devList[i].daqName);
   
		if (handle == -1)
		{
			printf("Error: Could not initialize %s.\n",devList[i].daqName);
		}
		else
		{
			printf("%s initialized.\n\n",devList[i].daqName);
			// close device
			daqClose(handle);
		}
   }
   
   printf("\n");
   printf("To exit press \"x\", any other key to continue\n>");

   char cIn;
  
   cIn = _getch();
   putchar(cIn);
   printf("\n");
   if (cIn == 'x')
   {
	   exit(0);		
   }

/******************************************************************************/
/*						ASYNCHRONOUS ACQUISITION SETUP						  */
/******************************************************************************/


	printf("\n-----------------------------------------\n");
	printf("Aysnchronous Acquisition Setup Parameters\n");
	printf("-----------------------------------------\n\n");
	printf("Number of Scans per board: %d\n", SCANS);
	printf("Scan Rate: %dHZ\n", RATE);
	printf("Number of files to be created: %d\n", devCount);

	// Maximum allowable input Voltage.
	float maxVolt;
	// Apply Scale and Offset to Analog input data.
	float scale[CHANCOUNT];
	float offset[CHANCOUNT];
	// conv_buffer stores converted analog input data values
	float *conv_buffer;
		
	// File pointer
	FILE* outputFile;

	// Connect to every available device and allocate memory for data structures.
	daqDevice* daqDevs = (daqDevice*)malloc(sizeof(daqDevice)*devCount);

	// "buffer" stores raw scan data from all installed DaqBoards	
	WORD	*buffer;

	// Allocate buffer based on # of devices, # of scans, and # of channels.
	buffer = (WORD*)malloc (sizeof(WORD)*devCount*SCANS*CHANCOUNT);

	// Allocate conv_buffer based on #
	conv_buffer = (float*)malloc (sizeof(float)*devCount*SCANS*CHANCOUNT);

	/*You must use a gain of X2 or greater when scanning unipolar, otherwise the resultant signal 
	  would allow maximum voltages 2x as high as the device maximum*/
	DaqAdcGain gains[CHANCOUNT] = {DgainX1, DgainX2};	
	DWORD channels[CHANCOUNT] = {0, 1};
	DWORD flags[CHANCOUNT] = {DafBipolar, DafBipolar};
	
	if (devCount)
	{
		// Set up all devices prior to scanning
		for (int i = 0; i < idevCount; i++)
		{
			printf("Setting up scan on device %d...\n", i);

			// Check DeviceProperties for Analog Input Channels
			daqGetDeviceProperties(devList[i].daqName, &devProps);
			
			if (devProps.mainUnitAdChannels > 0) 
			{
				// Boards with Analog Inputs
				daqDevs[i].handle = daqOpen(devList[i].daqName);
				daqAdcSetAcq(daqDevs[i].handle, DaamNShot, 0, SCANS);	

				// Scan settings
				daqAdcSetScan(daqDevs[i].handle, channels, gains, flags, CHANCOUNT);  
						
				// Set scan rate
		 		daqAdcSetFreq(daqDevs[i].handle, RATE);	

				// Set buffer location, size and flag settings
				daqAdcTransferSetBuffer(daqDevs[i].handle, buffer+i*SCANS*CHANCOUNT, SCANS, DatmUpdateSingle|DatmCycleOn);	
						
				// Set to Trigger on software trigger
				daqSetTriggerEvent(daqDevs[i].handle, STARTSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
	    							DaqTypeAnalogLocal, 0, 0, DaqStartEvent);

				// Set to Stop when the requested number of scans is completed
				daqSetTriggerEvent(daqDevs[i].handle, STOPSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
									DaqTypeAnalogLocal, 0, 0, DaqStopEvent);
			}
			else
			{
				// Boards with Digital Input Only set up P3 / P2 digital scan
				DWORD channels[CHANCOUNT] = {0,0};
				DaqAdcGain gains[CHANCOUNT] = {DgainX1,DgainX1};
				DWORD flags[CHANCOUNT] = {DafP3Local16,DafP2Local8};
			
				daqDevs[i].handle = daqOpen(devList[i].daqName);
	 			daqAdcSetAcq(daqDevs[i].handle, DaamNShot, 0, SCANS);

				// Scan settings
				daqAdcSetScan(daqDevs[i].handle, channels, gains, flags, CHANCOUNT);

				// Set scan rate
				daqAdcSetFreq(daqDevs[i].handle, RATE);
				
				// Set buffer location, size and flag settings
				daqAdcTransferSetBuffer(daqDevs[i].handle, buffer+i*SCANS*CHANCOUNT, SCANS, DatmUpdateSingle&DatmCycleOn);	
				
				// Set to Trigger on software trigger
				daqSetTriggerEvent(daqDevs[i].handle, STARTSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
	    							DaqTypeDigitalLocal, 0, 0, DaqStartEvent);
				
				// Set to Stop when the requested number of scans is completed
				daqSetTriggerEvent(daqDevs[i].handle, STOPSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
										DaqTypeDigitalLocal, 0, 0, DaqStopEvent);
			}
		}
	}

	// Trigger all devices
	printf("Scanning...\n");
	for (i = 0; i < idevCount; i++) 
	{
		// Begin data acquisition
		daqAdcTransferStart(daqDevs[i].handle);
		daqAdcArm(daqDevs[i].handle);
	}
	
	// Run while any are active
	do 
	{
		allActive=0;
		for (i = 0; i < idevCount; i++) 
		{
			// Transfer data into computer memory and halt acquisition when done
			daqAdcTransferGetStat(daqDevs[i].handle, &daqDevs[i].active, &daqDevs[i].retCount);
			allActive |= daqDevs[i].active;
		}
	}
	while ( allActive & DaafAcqActive );  

	// Disarm all when completed
	for (i = 0; i < idevCount; i++) 
	{	
		daqAdcDisarm(daqDevs[i].handle);
		printf("Scan Completed on device %d\n", i);
		totCount+=daqDevs[i].retCount;
	}

	/*Convert the data to volts manually:
	  DaqBoards convert all data to an unsigned, 16-bit number (range 0 to 65535.  Zero corresponds 
	  to the minimum voltage, which is -maxVolt if in bipolar mode, or zero if unipolar.  
	  65535 corresponds to maxVolt if bipolar, or 2xmaxVolt if unipolar.  Note that a voltage 
	  higher than the device's absolute range (+/-10V for DaqBoard2000, +/-5V for other Daq* devices)
	  can damage the device.  Setting flags and gain settings which indicate a voltage higher than 
	  the device's max, such as an unipolar, unscaled scan will result in an error before the scan 
	  is performed.*/

	for (i = 0; i < idevCount; i++) 
	{
		daqGetDeviceProperties(devList[i].daqName, &devProps);
		if(devProps.mainUnitAdChannels > 0)
		{
			// Boards with Analog Input - Open the file
			sprintf(file_name,"%s%d.txt",OUTPUTFILE,i);
			outputFile = fopen(file_name, "w+");

			// Write header information
			fprintf(outputFile, "Device %d: %s \tModel: %s\n", i, devList[i].daqName,daqTypeString[devProps.deviceType]);
			fprintf(outputFile, "Ch. 0\tCh. 1\n");

			
			/* Set the DaqBoard 2000 maximum input voltage and apply the proper scaling and offset factors
			for the polarity flags selected.*/
			maxVolt = 10.0;
			for(WORD q = 0; q < CHANCOUNT; q++)
			{
				/*(flag&DafBiplor) equals 2 if bipolar or 0 if unipolar
				scale should equal maxVolt/65335 for unipolar, maxVolt/32767 for bipolar*/
				scale[q] = maxVolt/(65535 - (flags[q]&DafBipolar)*16384);
				
				// offset should equal maxVolt for bipolar, 0 for unipolar
				offset[q] = maxVolt*(flags[q]&DafBipolar)/2;
			}

			// Fill conv_buffer with scaled data
			for (WORD t = 0; t < daqDevs[i].retCount*CHANCOUNT; t++)
				(conv_buffer+i*SCANS*CHANCOUNT)[t] = (buffer+i*SCANS*CHANCOUNT)[t]*scale[t%CHANCOUNT] - offset[t%CHANCOUNT];
			
			// Write data
			for(WORD k = 0; k < daqDevs[i].retCount; k++)
			{
				for(WORD j = 0; j < CHANCOUNT; j++)
					fprintf(outputFile, "%1.3f\t", (conv_buffer+i*SCANS*CHANCOUNT)[(CHANCOUNT * k) + j]);
					fprintf(outputFile, "\n");
			}
				// Close the file
				fclose(outputFile);
		 }
		 else
		 { 
				// Boards with Digital Input Only - Open the file and write data
				sprintf(file_name,"%s%d.txt",OUTPUTFILE,i);
				outputFile = fopen(file_name, "w+");
				fprintf(outputFile, "Device %d: %s \tModel: %s\n", i, devList[i].daqName,daqTypeString[devProps.deviceType]);
				fprintf(outputFile, "P3 Dig\tP2 Dig\n");
				for(WORD k = 0; k < daqDevs[i].retCount; k++)
				{
					for(WORD j = 0; j < CHANCOUNT; j++)
					fprintf(outputFile, "%d\t", (buffer+i*SCANS*CHANCOUNT)[(CHANCOUNT * k) + j]);
					fprintf(outputFile, "\n");
				}
				// Close the file
				fclose(outputFile);
		 }
	}
			
	// Update screen
	printf("\nCompleted %d scans\n\n", totCount);
	printf("Acquired data has been saved in the files listed below...\n");
	for (i = 0; i < idevCount;i++)
	{
		printf("%s%d.txt\n", OUTPUTFILE, i);
	}
		
	printf("\n");
	printf("To exit press \"x\", any other key to continue:\n>");
	cIn = _getch();
	putchar(cIn);
	printf("\n");
	if (cIn == 'x')
	{
		// Close device connections
		for (i = 0; i < idevCount; i++)
		{
			daqClose(daqDevs[i].handle);	
		}

		// Free memory and exit
		free(buffer);
	    free(conv_buffer);
	    free(daqDevs);
		exit(0);
	}

/******************************************************************************/
/*						SYNCHRONOUS ACQUISITION SETUP						  */
/******************************************************************************/
	
	// Update screen
	printf("\n-----------------------------------------\n");
	printf("Synchronous Acquisition Setup Parameters\n");
	printf("-----------------------------------------\n\n");
	printf("Number of Scans per board: %d\n", SCANS);
	printf("Scan Rate: %dHZ\n", RATE);
	printf("Number of files to be created: 1\n\n");
	printf("Configuration:\tMaster / Slave\n\n");

	// Inform user about hardware requirements
	printf("THIS CONFIGURATION REQUIRES THAT ALL PACER CLOCK PINS, (A/I CLK-P1 pin 20), BE\nCONNECTED TOGETHER.\n\n");

	// Inform user about master daqboard
	daqGetDeviceProperties(devList[0].daqName, &devProps);
	printf("The Master board for the synchronous acquisition will be Device 0.\n");
	printf("Device Type:\t%s\n\n", daqTypeString[devProps.deviceType]);
	// Last chance
	printf("BEFORE CONTINUING PLEASE VERIFY THAT ALL SLAVE ACQUISITION PACER CLOCKS,\n(A/I CLK-P1 pin 20), ARE CONNECTED TO THE MASTER PACER (A/I CLK-P1 pin 20)\n\n");
	
	printf("To exit press \"x\", any other key to continue:\n>");
	cIn = _getch();
	putchar(cIn);
	printf("\n");
	if (cIn == 'x')
	{
	   	// Close device connections
		for (i = 0; i < idevCount; i++)
		{
			daqClose(daqDevs[i].handle);	
		}

		// Free memory and exit
		free(buffer);
	    free(conv_buffer);
	    free(daqDevs);
		exit(0);
	}

	// Reset total count
	totCount = 0; 
	if (devCount)
	{
		// Set up all devices prior to scanning
		for (int i = 0;  i < idevCount; i++)
		{
			printf("Setting up scan on device %d...\n", i);
	
			// Check DeviceProperties for Analog Input Channels
			daqGetDeviceProperties(devList[i].daqName, &devProps);
		
			if (devProps.mainUnitAdChannels > 0)
			{
				// Boards with Analog Inputs
				daqDevs[i].handle = daqOpen(devList[i].daqName);
				daqAdcSetAcq(daqDevs[i].handle, DaamNShot, 0, SCANS);
				
				//Scan settings
				daqAdcSetScan(daqDevs[i].handle, channels, gains, flags, CHANCOUNT);  
						
				// Set scan rate
				daqAdcSetFreq(daqDevs[i].handle, RATE);	

				// Set buffer location, size and flag settings
				daqAdcTransferSetBuffer(daqDevs[i].handle, buffer+i*SCANS*CHANCOUNT, SCANS, DatmUpdateSingle|DatmCycleOn);	
						
				// Set to Trigger on software trigger
				daqSetTriggerEvent(daqDevs[i].handle, STARTSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
	    							DaqTypeAnalogLocal, 0, 0, DaqStartEvent);
				//Set to Stop when the requested number of scans is completed
		 		daqSetTriggerEvent(daqDevs[i].handle, STOPSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
									DaqTypeAnalogLocal, 0, 0, DaqStopEvent);
			}
			else
			{
				// Boards with Digital Input Only set up P3 / P2 digital scan
				DWORD channels[CHANCOUNT] = {0,0};
				DaqAdcGain gains[CHANCOUNT] = {DgainX1,DgainX1};
				DWORD flags[CHANCOUNT] = {DafP3Local16,DafP2Local8};
			
				daqDevs[i].handle = daqOpen(devList[i].daqName);
				daqAdcSetAcq(daqDevs[i].handle, DaamNShot, 0, SCANS);

				// Scan settings
				daqAdcSetScan(daqDevs[i].handle, channels, gains, flags, CHANCOUNT);
			
				// Set scan rate
		 		daqAdcSetFreq(daqDevs[i].handle, RATE);	
			
				// Set buffer location, size and flag settings
				daqAdcTransferSetBuffer(daqDevs[i].handle, buffer+i*SCANS*CHANCOUNT, SCANS, DatmUpdateSingle&DatmCycleOn);	
			
				// Set to Trigger on software trigger
				daqSetTriggerEvent(daqDevs[i].handle, STARTSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
	    						DaqTypeDigitalLocal, 0, 0, DaqStartEvent);
				// Set to Stop when the requested number of scans is completed
				daqSetTriggerEvent(daqDevs[i].handle, STOPSOURCE, (DaqEnhTrigSensT)NULL, channels[0], gains[0], flags[0],
								DaqTypeDigitalLocal, 0, 0, DaqStopEvent);
			}
		}
	}

	// Set device 0 to output clock to synchronize all other devices
	daqAdcSetClockSource(daqDevs[0].handle, (DaqAdcClockSource)(DacsAdcClock | DacsOutputEnable));
		
	// Set external clock for all other devices
	for (i = 1; i < idevCount; i++)
	{
		daqAdcSetClockSource(daqDevs[i].handle, DacsExternalTTL);
	}
	
	// Arm Slave devices
	for (i = 1; i < idevCount; i++) 
	{
		daqAdcTransferStart(daqDevs[i].handle);
		daqAdcArm(daqDevs[i].handle);
	}
		
	// Arm Master and begin acquisition
	daqAdcTransferStart(daqDevs[0].handle);
	daqAdcArm(daqDevs[0].handle);

	printf("Scanning...\n");
	
	// Run while any are active
	do 
	{
		allActive=0;
		for (i = 0; i < idevCount; i++) 
		{
			// Transfer datainto computer memory and halt acquisition when done
			daqAdcTransferGetStat(daqDevs[i].handle, &daqDevs[i].active, &daqDevs[i].retCount);
			allActive |= daqDevs[i].active;
		}
	}
		
	while ( allActive & DaafAcqActive );  
	//Disarm all when completed
	for (i = 0; i < idevCount; i++) 
	{	
		daqAdcDisarm(daqDevs[i].handle);
		printf("Scan Completed on device %d\n", i);
		totCount+=daqDevs[i].retCount;
	}
	
	// Open the Output File data will be saved to
	sprintf(file_name,"%s.txt", OUTPUTFILE_SYNCH);
	outputFile = fopen(file_name, "w+");
	
	for (i = 0; i < idevCount; i++)
	{		
		// Write header information starting from board 0
		daqGetDeviceProperties(devList[i].daqName, &devProps);
		fprintf(outputFile, "Device %d: %s \tModel: %s\t", i, devList[i].daqName,daqTypeString[devProps.deviceType]);	
	}

	fprintf(outputFile, "\n");

	for (i = 0; i < idevCount; i++)
	{		
		daqGetDeviceProperties(devList[i].daqName, &devProps);

		// Check for analog inputs
		if (devProps.mainUnitAdChannels > 0)
		{
			// Boards with analog inputs - write channel labels
			fprintf(outputFile, "Ch. 0\tCh. 1\t");
		}
		else
		{
			// Boards with digital inputs - write channel labels
			fprintf(outputFile, "P3 Dig\tP2 Dig\t");
		}
	}

	fprintf(outputFile, "\n");

	for (i = 0; i < idevCount; i++)
	{
		daqGetDeviceProperties(devList[i].daqName, &devProps);
		if(devProps.mainUnitAdChannels > 0)
		{
			/*Analog inputs
			The following routine automatically determines the mavoltage for the device used
			and the proper scaling and offset factors for the polarity flags selected.*/

			//DaqBoard 2000 maximum voltage  
			maxVolt = 10.0;
			for(WORD q = 0; q < CHANCOUNT; q++)
			{
				/*(flag&DafBiplor) equals 2 if bipolar or 0 if unipolar
				scale should equal maxVolt/65335 for unipolar, maxVolt/32767 for bipolar*/
				scale[q] = maxVolt/(65535 - (flags[q]&DafBipolar)*16384);
				
				// offset should equal maxVolt for bipolar, 0 for unipolar
				offset[q] = maxVolt*(flags[q]&DafBipolar)/2;
			}

			// Fill conv_buffer with scaled data
			for (WORD t = 0; t < daqDevs[i].retCount*CHANCOUNT; t++) 
			{
				(conv_buffer+i*SCANS*CHANCOUNT)[t] = (buffer+i*SCANS*CHANCOUNT)[t]*scale[t%CHANCOUNT] - offset[t%CHANCOUNT];
			}
		}
	}
			
	// Get device 0's return count to use for loop index
	int itotCount = daqDevs[0].retCount;
	for (int y = 0; y < itotCount; y++)
	{
		for (i = 0; i < idevCount; i++)
		{
			daqGetDeviceProperties(devList[i].daqName, &devProps);
			if(devProps.mainUnitAdChannels > 0)
			{
				// Analog inputs
				for(WORD j = 0; j < CHANCOUNT; j++)
				{
					// Write data
					fprintf(outputFile, "%1.3f\t", (conv_buffer+i*SCANS*CHANCOUNT)[(CHANCOUNT * y) + j]);
				}
			}
			else
			{	
				// Digital inputs
				for(WORD j = 0; j < CHANCOUNT; j++)
				{
					// Write data
					fprintf(outputFile, "%d\t", buffer[(i*SCANS*CHANCOUNT)+((CHANCOUNT * y) + j)]);
				}
			}
		}
	fprintf(outputFile, "\n");
	}

	fprintf(outputFile, "\n");

	if (i == idevCount -1) 
	{
		// If device count has been reached - reset
		i = 0;
	}
			
	// Update screen
	printf("\nCompleted %d scans\n\n", totCount);
	printf("Acquired data has been saved in the file listed below...\n");
	
	printf("%s.txt\n", OUTPUTFILE_SYNCH);
	// Close the file
	fclose(outputFile);
	printf("Closing all DaqBoards...");
	
	for (i = 0; i < idevCount; i++)
	{
		// Close all devices
		daqClose(daqDevs[i].handle);	
	}
	printf("Complete\n");	

	// Free Memory
	free(buffer);
	free(daqDevs);
	free(conv_buffer);

   // End of example
   printf("\n\nEnd of Example...\n");
	printf("\n\n<hit any key to exit>\n");
   _getch();
}
